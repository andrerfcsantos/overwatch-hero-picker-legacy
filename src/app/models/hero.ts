import { Role } from './role';

export class Hero {
    name: string;
    role: Role;
    selected: boolean;
    key?: string;
}
